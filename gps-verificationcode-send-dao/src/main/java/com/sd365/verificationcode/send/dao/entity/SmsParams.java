package com.sd365.verificationcode.send.dao.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ProjectName: gps-verificationcode-send-service
 * @Package: com.sd365.verificationcode.send.dao.entity
 * @ClassName: SmsParams
 * @Description: java类作用描述
 * @Author: yuye
 * @CreateDate: 2021/1/23 12:42
 * @UpdateDate: 2021/1/23 12:42
 * @Version: 1.0
 */
@Data
@Accessors(chain = true)
public class SmsParams {

    /**
     * 验证码
     */
    private String verifyCode;

    /**
     * 手机号码
     */
    private String phone;


    public SmsParams(String phone, String verifyCode) {
        this.phone = phone;
        this.verifyCode = verifyCode;
    }
}
