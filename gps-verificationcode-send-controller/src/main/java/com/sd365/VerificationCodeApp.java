package com.sd365;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * The type Procurement acceptance app.
 *
 * @ProjectName: procurement -regulation-service
 * @Package: com.sd365
 * @ClassName: ProcurementAcceptanceApp
 * @Description: 采购监管验收启动类
 * @Author: yuye
 * @CreateDate: 2020 /12/28 17:10
 * @UpdateDate: 2020 /12/28 17:10
 * @Version: 1.0
 */
@SpringBootApplication
@ComponentScan("com.sd365.common")
@ComponentScan("com.sd365.common.api.version")
//@MapperScan(("com.sd365.procurement.acceptance.dao.mapper"))
//开启注解支持
@EnableCaching
@EnableSwagger2
//@EnableDiscoveryClient
//@EnableRabbit
//@EnableTransactionManagement
//@EnableFeignClients
////添加熔断降级注解
//@EnableCircuitBreaker
/**
 * @Description:   procurement-acceptance-service微服务启动类
 * @Author: yuye
 * @Date: 2021/1/21
 */
public class VerificationCodeApp {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main( String[] args )
    {
        SpringApplication.run(VerificationCodeApp.class, args);
    }
}
