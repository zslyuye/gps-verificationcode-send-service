package com.sd365.verificationcode.send.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.verificationcode.send.serve.SmsService;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ProjectName: gps-verificationcode-send-service
 * @Package: com.sd365.verificationcode.send.api.controller
 * @ClassName: SmsServiceController
 * @Description: SmsService实现方法
 * @Author: yuye
 * @CreateDate: 2021/1/23 14:22
 * @UpdateDate: 2021/1/23 14:22
 * @Version: 1.0
 */
@RestController
@RequestMapping(value = "gps/verificationcode/send/sms")
@CrossOrigin
public class SmsServiceController  {

    /**
     * The type Response data.
     */
    @Data
    class SmsServiceData {
        private Boolean statu;
        private int currentcode;
        private String message;
    }

    @Autowired
    private SmsService smsService;


    @ApiOperation(tags="短信验证发送",value="")
    @GetMapping(value = "")
    @ResponseBody
    @ApiLog
    public SmsServiceData sendSmsCode(@RequestParam("phone") String phone) {
        SmsServiceData smsServiceData = new SmsServiceData();
        if(smsService.sendSmsCode(phone)){
            smsServiceData.statu = true;
            smsServiceData.currentcode = 200;
            smsServiceData.message = "验证码发送成功";
        } else{
            smsServiceData.statu = false;
            smsServiceData.currentcode = 500;
            smsServiceData.message = "验证码发送失败";
        }
        return smsServiceData;
    }

    @ApiLog
    @ApiOperation(tags="短信电话验证匹配",value="")
    @GetMapping(value = "/verifyCode")
    @ResponseBody
    public SmsServiceData verifyCode(@RequestParam("phone") String phone,@RequestParam("code") String code){
        SmsServiceData smsServiceData = new SmsServiceData();
        if(smsService.verifyCode(phone,code)){
            smsServiceData.statu = true;
            smsServiceData.currentcode = 200;
            smsServiceData.message = "短信电话验证匹配成功";
        } else{
            smsServiceData.statu = false;
            smsServiceData.currentcode = 500;
            smsServiceData.message = "短信电话验证匹配失败";
        }
        return smsServiceData;
    }
}
