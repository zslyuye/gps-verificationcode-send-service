package com.sd365.verificationcode.send.serve.impl;

import com.sd365.verificationcode.send.serve.SmsService;
import com.sd365.verificationcode.send.serve.config.SmsProperties;
import com.sd365.verificationcode.send.serve.sendenum.SmsLengthEnum;
import com.sd365.verificationcode.send.serve.sendenum.SmsResponseCodeEnum;
import com.sd365.verificationcode.send.serve.util.SmsCodeUtil;
import com.sd365.verificationcode.send.serve.util.SmsUtil;
import com.tencentcloudapi.sms.v20190711.models.SendStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @ProjectName: gps-verificationcode-send-service
 * @Package: com.sd365.verificationcode.send.serve.impl
 * @ClassName: SmsServiceImpl
 * @Description: 短信业务
 * @Author: yuye
 * @CreateDate: 2021/1/23 13:40
 * @UpdateDate: 2021/1/23 13:40
 * @Version: 1.0
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class SmsServiceImpl implements SmsService {

    @Autowired
    private RedisTemplate redisTemplate;

    private final SmsProperties smsProperties;

    /** 腾讯云短信模板id-短信验证码 */
    @Value("${sms-config.templateIds.code}")
    private String templateIdCode;

    /**
     * description: 发送短信验证码
     *
     * @param phone 手机号
     * @author yuye
     * Date: 2020/9/16 10:11
     */
    @Override
    public Boolean sendSmsCode(String phone) {
        // redis缓存的键
        final String smsKey = SmsCodeUtil.createSmsCacheKey(smsProperties.getPhonePrefix(), phone, "wxUser_sign");

        //下发手机号码，采用 e.164 标准，+[国家或地区码][手机号]  示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号
        String[] phoneNumbers = {"+86" + phone};
        //模板参数: 若无模板参数，则设置为空（第一个参数为随机验证码，第二个参数为有效时间）
        final String smsRandomCode = SmsCodeUtil.createSmsRandomCode(SmsLengthEnum.SMS_LENGTH_4);
        String[] templateParams = {smsRandomCode};
        SendStatus[] sendStatuses = SmsUtil.sendSms(smsProperties, templateIdCode, templateParams, phoneNumbers);
        String resCode = sendStatuses[0].getCode();
        if (resCode.equals(SmsResponseCodeEnum.OK.getCode())) {
            // 返回ok，缓存
            redisTemplate.opsForValue().set(smsKey,smsRandomCode,60,TimeUnit.SECONDS);
            log.info("短信发送成功");
            return true;
//            redisUtils.set(smsKey, smsRandomCode, Long.parseLong(smsProperties.getExpireTime()), TimeUnit.MINUTES);
        } else {
            log.error("【短信业务-发送失败】phone:" + phone + "errMsg:" + sendStatuses[0].getMessage());
            return false;
        }



    }

    /**
     * description:验证短信验证码
     *
     * @param phone 手机号
     * @param code  验证码
     * @author yuye
     * Date: 2020/9/16 10:11
     */
    @Override
    public Boolean verifyCode(String phone, String code) {
        // redis缓存的键
        final String smsKey = SmsCodeUtil.createSmsCacheKey(smsProperties.getPhonePrefix(), phone,
                "wxUser_sign");

        if(code.equals(redisTemplate.opsForValue().get(smsKey))){
            log.info("【短信业务-微信公众号手机认证成功】phone：" + phone);
            return true;
        }else{
            log.error("【短信业务-微信公众号手机认证失败】验证码失效。phone：" + phone);
            return false;
        }
    }

}
