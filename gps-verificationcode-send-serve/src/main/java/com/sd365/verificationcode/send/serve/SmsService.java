package com.sd365.verificationcode.send.serve;

/**
 * description: 短信业务
 *
 * @author yuye
 * Date: 2020/9/15 22:06
 **/
public interface SmsService {

    /**
     * description: 发送短信验证码
     *
     * @param phone 手机号
     * @author RenShiWei
     * Date: 2020/9/16 10:11
     */
    Boolean sendSmsCode(String phone);

    /**
     * description:验证短信验证码
     *
     * @param phone 手机号
     * @param code  验证码
     * @author RenShiWei
     * Date: 2020/9/16 10:11
     */
    Boolean verifyCode(String phone, String code);

}