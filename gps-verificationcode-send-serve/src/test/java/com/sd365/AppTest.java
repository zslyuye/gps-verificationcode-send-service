package com.sd365;

import static org.junit.Assert.assertTrue;

import com.sd365.verificationcode.send.serve.SmsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * description: 测试腾讯云SDK3.0
 *
 * Date: 2020/9/16 9:10
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class AppTest 
{
    @Autowired
    private SmsService smsService;

    @Test
    public void testSendCode() {
        smsService.sendSmsCode("17370836805");
    }
}
